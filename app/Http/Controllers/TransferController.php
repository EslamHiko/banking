<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Transfer;

class TransferController extends Controller
{
    public function index(){
      return view('transfers');
    }

    public function transferView(){

      return view('single-transfer');
    }

    public function createTransfer(Request $request){
      $amount = $request['amount'];
      $from_id = $request['from_id'];
      $to_id = $request['to_id'];
      $from = Customer::findOrFail($from_id);
      $to = Customer::findOrFail($to_id);
      if($amount > $from->balance){
        return redirect()->back()->with(['error'=>true,'msg'=>'wrong amount']);
      }
      $from->balance = $from->balance - $amount;
      $to->balance = $to->balance + $amount;
      $transfer = new Transfer(['from_id'=>$from_id,'to_id'=>$to_id,'amount'=>$amount]);
      $transfer->save();
      $from->save();
      $to->save();
      return redirect()->back()->with(['success'=>true,'msg'=>'successful operation']);


    }
}
