<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Customer;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->double('balance')->default(0);
            $table->timestamps();
        });

        $customers = [
          ['name' => 'aya', 'email' => 'ayaabuelsoud98@gmail.com', 'phone' => '0123456789', 'balance' => 150000],
          ['name' => 'eslam', 'email' => 'eslam@gmail.com', 'phone' => '0123456789', 'balance' => 100000],
          ['name' => 'abdallah', 'email' => 'ayaabuelsoud98@gmail.com', 'phone' => '0123456789', 'balance' => 50000],
        ];

        foreach ($customers as $customer) {
          $customerRecord = new Customer($customer);
          $customerRecord->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
