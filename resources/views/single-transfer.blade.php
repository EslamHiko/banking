@extends('layouts.dashboard')

@section('content')
  <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">

  <form method="post" enctype="multipart/form-data" action="{{url('/transfer')}}">
    @csrf
  <div class="mb-3">
    <label for="exampleDataList" class="form-label">From</label>
  <input class="form-control" list="datalistOptions" name="from_id" id="exampleDataList" placeholder="Type to search...">
  <datalist id="datalistOptions">
    @foreach (\App\Models\Customer::all() as $customer)
      <option value="{{$customer->id}}">{{$customer->name}}</option>
    @endforeach

  </datalist>
  </div>
  <div class="mb-3">
    <label for="exampleDataList" class="form-label">To</label>
  <input class="form-control" list="datalistOptions2" name="to_id" id="exampleDataList2" placeholder="Type to search...">
  <datalist id="datalistOptions2">
    @foreach (\App\Models\Customer::all() as $customer)
      <option value="{{$customer->id}}">{{$customer->name}}</option>
    @endforeach

  </datalist>
  </div>
  <div class="mb-3">
  <label for="exampleInputEmail1" class="form-label">Amount</label>
  <input type="number" class="form-control" min="1" name="amount" aria-describedby="emailHelp">
</div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</main>

@endsection
