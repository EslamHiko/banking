<!DOCTYPE html>
<!-- saved from url=(0053)https://getbootstrap.com/docs/5.1/examples/dashboard/ -->
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <title>Dashboard Template · Bootstrap v5.1</title>




    <!-- Bootstrap core CSS -->
<link href="{{url('css/bootstrap.min.css')}}" rel="stylesheet" >


    <!-- Custom styles for this template -->
    <link href="{{url("dashboard.css")}}" rel="stylesheet">
</head>
  <body data-new-gr-c-s-check-loaded="14.1049.0" data-gr-ext-installed="">
    @include('components.header')
    <div class="container-fluid">
  <div class="row">
    @include('components.side-bar')
    @yield('content')
  </div>
</div>

  <script src="{{url("js/bootstrap.bundle.min.js")}}"></script>
    </body>
    </html>
